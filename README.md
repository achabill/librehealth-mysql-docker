# LibreHealth Toolkit MySQL Docker

> Docker containers for [LibreHealth](http://librehealth.io) Toolkit MySQL

This repository contains the necessary code to create Docker containers that start an instance
of the LibreHealth Toolkit MySQL database.

For more information about the LibreHealth Toolkit visit [librehealth.io](http://www.librehealth.io/).

## Prerequisites

Make sure you have [Docker](https://docs.docker.com/) installed.

## Setup
You need to have the Docker image locally in order to run it.

You can either build it yourself or pull it from the :cloud:

Please follow the appropriate section below.

### Build
Start by cloning this repository:

```
git clone https://gitlab.com/achabill/librehealth-mysql-docker.git
```

Enter the directory and build the image:

```
cd librehealth-mysql-docker
docker build --tag librehealth-mysql .
```

### Pull
The Docker image is hosted on [Docker Hub](https://hub.docker.com/r/achabill/lh-mysql/)

You can pull via
```
docker pull achabill/lh-mysql
```

### Run
Ensure you built or pulled the image first.

Run an instance of the image (LibreHealth Toolkit database):

```
docker run -d librehealth-mysql -p 3306:3306 librehealth-mysql-docker --name librehealthdb \
    -e MYSQL_ROOT_PASSWORD="root" \
    -e MYSQL_USER="librehealth" \
    -e MYSQL_PASSWORD="librehealth" \
    -e MYSQL_DATABASE="librehealth"
```

### List
If you want to find your image list all images and look for your tag:

```
docker images
```

## License

[MPL 2.0 w/ HD](http://openmrs.org/license/) © [OpenMRS Inc.](http://www.openmrs.org/)